from sentence_transformers import SentenceTransformer, util
import pandas as pd

# text_corpus: collection of text, every row contains a new text element
# model_name [STRING]: model name from the huggingface model library

# Output: A DataFrame containing each pairwise distance between the given sentences. 

def semantic_similarity(text_corpus=[], model_name='roberta-large-nli-stsb-mean-tokens'):
    model = SentenceTransformer(model_name) 
    number_of_sentences = len(text_corpus) 

    # Query sentences:
    # Single list of sentences - Possible tens of thousands of sentences
    # sentences = corpus[:][0:number_of_sentences]

    paraphrases = util.paraphrase_mining(model, text_corpus, top_k=number_of_sentences, show_progress_bar=True,
                                         max_pairs=number_of_sentences * number_of_sentences, batch_size=1)
    df_result = pd.DataFrame(paraphrases)
    df_result.columns = ['score', 'sentence_index', 'sentence_index_compared']
    df_result.to_csv('paraphrase_mining_results.csv', index=False)
    # start counting with 1 like R does it
    df_result.sentence_index += 1
    df_result.sentence_index_compared += 1
    return df_result
