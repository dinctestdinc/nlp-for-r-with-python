# Disclaimer
The whole repository is work-in-progress. Please use it cautiously!

## Introduction
This repository helps you to use state-of-the-art language models in R like BERT, ROBERTA, DistilBERT or XLNet. The models are loaded via Python. Python is also used to calculate the semantic similarities between given text corpi. All the analysis is then performed in R.

## Requirements for Python
You need the Anaconda environment. This will enable you to use the huggingface model base (https://huggingface.co/models). These models can calculate semantic similarities between different text corpi, derive the sentiment of a sentence or text, and many more. 

Therefore: 

- Install the Anaconda environment (https://docs.anaconda.com/anaconda/install/). 

Once the Anaconda environment has been installed. Go into the terminal and activate the environment with `conda activate [NAME OF YOUR CONDA ENVIRONMENT]`. Then run `pip install [NAME OF THE PACKAGE]` to install the following packages:

- sentence-transformers package (https://pypi.org/project/sentence-transformers).
- transformers package (https://pypi.org/project/transformers/).

Once everything has been installed you can deactivate the anaconda environment with `conde deactivate`.

## Tutorial

### Sentiment analysis
To perform a sentiment analysis run the **sentiment_analysis.R**. By default it predicts the sentiment of a given sentence or text as a number of stars (between 1 and 5). The output of the R file is:

| sentiment | score | text | index |
| ------ | ------ | ------ | ------ |
| 5 stars | 0.7237318 | I love football. | 1 |
| 1 star | 0.8804366 | Football is the worst game on earth. | 2 |
| 2 stars | 0.4480275 | The app used to be great. But now it has become unusable. | 3 |

The score measures the confidence of the model regarding the predicted rating. The score takes values between 0 and 1.

### Semantic similarity
To perform a semantic similarity analysis run the **semantic_similiary.R**. This code will calculate the pairwise similarity between each sentence or text. The higher the estimated score the more similar are the sentences to each other. For example:
If the following five sentences were given:

| index | text | 
| ------ | ------ | 
| 1 | I love football. | 
| 2 | Football is the worst game on earth. |
| 3 | I like football very much. | 
| 4 | Football is not interesting for me. | 
| 5 | The weather is sunny today. | 

Then the corresponding similarities are (estimated with the large [ROBERTA model](https://huggingface.co/sentence-transformers/roberta-large-nli-stsb-mean-tokens)):

| score | sentence_index | sentence_index_compared
| ------ | ------ |  ------ |  
| 0.925  | 1 | 3 |    
| 0.523  | 2 | 4 |     
| 0.513  | 3 | 4 |    
| 0.465  | 1 | 4 |      
| 0.449  | 2 | 3 |     
| 0.447  | 1 | 2 |
| 0.251  | 3 | 5 |    
| 0.174  | 4 | 5 |     
| 0.130  | 1 | 5 |    
| 0.0788 | 2 | 5 |

One can already see, that the first and the third sentence have a very high similarity of 0.925. This table is saved in "paraphrase_mining_results.csv". You can use this table afterward, since processing many sentences can take a while. 
We can visualize all similarities in a network plot and cluster them using the fast greedy cluster algorithm:

![Network graph](/resources/network_plot.png)

The cluster algorithm groups all sentences about the topic "football" together. The sentence about the "weather" is a clear outlier in this set of sentences. Interestingly, both sentences with a positive (negative) sentiment are closer to each other than to the remaining sentences.

### Future work
I will constantly update this repository adding more features. I am planning to add the possibility to compare different NLP models and rank the sentences according to their importance using Google's pagerank algorithm.

### Picture source
The repository's picture has been downloaded from www.flaticon.com.

### Donation
This work took several hours to complete. If you like to support me and found this code helpful, please buy me a cup of creativity-enhancing coffee:
<a href="https://www.buymeacoffee.com/dbendel" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-green.png" alt="Buy Me A Coffee" style="height: 30px !important;width: 108px !important;" ></a>
