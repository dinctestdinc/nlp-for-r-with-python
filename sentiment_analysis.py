from transformers import AutoTokenizer, AutoModelForSequenceClassification, pipeline
import pandas as pd

# model_name [STRING]: The name of the sentiment models from the huggingface model library.  
# sentence [STRING]: The sentence or text corpus to analyse. 
# index [INTEGER]: If mulitiple sentences should be analysed, this index stores the position of the corresponding text input. 

def sentiment_analysis(sentence="I love you.", index=1, model_name='nlptown/bert-base-multilingual-uncased-sentiment'):

    model = AutoModelForSequenceClassification.from_pretrained(model_name)
    tokenizer = AutoTokenizer.from_pretrained(model_name, max_length=512)
    classifier = pipeline('sentiment-analysis', model=model, tokenizer=tokenizer)

    results = classifier(sentence)
    df_result = pd.DataFrame(results)
    df_result.columns = ['sentiment', 'score']
    df_result['text'] = sentence
    df_result['index'] = index

    return df_result
